package com.milanghimire.loginandregister.di

import android.content.Context
import androidx.room.Room
import com.milanghimire.loginandregister.db.AppDatabase
import com.milanghimire.loginandregister.utils.Constants.APP_DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class) // Application Component is use coz application needs the DB object. There are ActivityComponent and FragmentComponent too
object AppModule {

    @Singleton // Tell Dagger-Hilt to create a singleton accessible everywhere in Application Component (i.e. everywhere in the application)
    @Provides
    fun provideAppDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        AppDatabase::class.java,
        APP_DATABASE_NAME
    ).build()

    @Singleton
    @Provides
    fun provideAppDao(db: AppDatabase) = db.getAppDao()
}