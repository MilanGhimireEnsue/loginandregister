package com.milanghimire.loginandregister.repository

import com.milanghimire.loginandregister.db.AppDao
import com.milanghimire.loginandregister.entities.User
import javax.inject.Inject

/*
* Repository is used for collecting data from all data sources
* */
class AppDbRepository @Inject constructor(
    private val appDao: AppDao
) {

    suspend fun insertUser(user: User) = appDao.upsert(user)

    suspend fun getUser(username: String, password: String) = appDao.getUser(username, password)

    /*
    * Haven't used suspend coz LiveData object is itself asynchronous
    * */
    fun getUsers() = appDao.getAllUsers()

    suspend fun checkUsername(username: String) = appDao.checkUsername(username)

}