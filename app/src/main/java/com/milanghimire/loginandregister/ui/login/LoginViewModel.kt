package com.milanghimire.loginandregister.ui.login

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.milanghimire.loginandregister.entities.User
import com.milanghimire.loginandregister.repository.AppDbRepository
import com.milanghimire.loginandregister.utils.Resource
import kotlinx.coroutines.launch
import java.lang.Exception

class LoginViewModel @ViewModelInject constructor(
    private val appDbRepository: AppDbRepository
) : ViewModel() {

    var loginStatus: MutableLiveData<Resource<String>> = MutableLiveData()

    fun doLogin(username: String, password: String) = viewModelScope.launch {
        try {
            loginStatus.postValue(Resource.Loading())
            if (appDbRepository.getUser(username, password) > 0) {
                loginStatus.postValue(Resource.Success("Login Successfully."))
            } else {
                loginStatus.postValue(Resource.Error("Username or password didn't matched."))
            }
        } catch (ex: Exception) {
            Log.e("LoginViewModel", "doLogin Exception Occurred: ${ex.message}")
            loginStatus.postValue(Resource.Error("Username or password didn't matched."))
        }
    }

    fun getUsersList() : LiveData<List<User>> {
        return appDbRepository.getUsers()
    }
}