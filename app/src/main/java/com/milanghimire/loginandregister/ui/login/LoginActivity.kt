package com.milanghimire.loginandregister.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.milanghimire.loginandregister.databinding.ActivityLoginBinding
import com.milanghimire.loginandregister.db.AppDao
import com.milanghimire.loginandregister.ui.register.RegisterActivity
import com.milanghimire.loginandregister.utils.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private lateinit var loginActivityBinding: ActivityLoginBinding

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginActivityBinding = ActivityLoginBinding.inflate(layoutInflater)
        val view = loginActivityBinding.root
        setContentView(view)

        initClickListener()
    }
//    https://stackoverflow.com/questions/63146318/how-to-create-and-use-a-room-database-in-kotlin-dagger-hilt
//    https://developer.android.com/training/dependency-injection/hilt-android
//    https://medium.com/@svvashishtha/using-room-with-hilt-cb57a1bc32f

    private fun initClickListener() {
        loginActivityBinding.btnLogin.setOnClickListener {
            if (formIsValid()) processLogin()
        }

        loginActivityBinding.tvBtnRegister.setOnClickListener {
            Intent(this@LoginActivity, RegisterActivity::class.java).also {
                startActivity(it)
            }
        }
    }

    private fun formIsValid(): Boolean {
        if (loginActivityBinding.etName.text.toString().isNullOrEmpty()) {
            Toast.makeText(
                this@LoginActivity,
                "Username field is empty",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        if (loginActivityBinding.etPassword.text.toString().isNullOrEmpty()) {
            Toast.makeText(
                this@LoginActivity,
                "Password field is empty",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        return true
    }

    private fun processLogin() {
        viewModel.doLogin(
            loginActivityBinding.etName.text.toString(),
            loginActivityBinding.etPassword.text.toString()
        )
        viewModel.loginStatus.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    Toast.makeText(this@LoginActivity, response.data, Toast.LENGTH_SHORT).show()
                }

                is Resource.Error -> {
                    Toast.makeText(this@LoginActivity, response.message, Toast.LENGTH_SHORT).show()
                }

                is Resource.Loading -> {
                    // in loading state
                }
            }
        })
    }
}