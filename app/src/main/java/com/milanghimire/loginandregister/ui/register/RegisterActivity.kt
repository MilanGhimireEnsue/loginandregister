package com.milanghimire.loginandregister.ui.register

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.milanghimire.loginandregister.databinding.ActivityRegisterBinding
import com.milanghimire.loginandregister.repository.AppDbRepository
import com.milanghimire.loginandregister.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {

    private lateinit var registerActivityBinding: ActivityRegisterBinding

    private val registerViewModel: RegisterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerActivityBinding = ActivityRegisterBinding.inflate(layoutInflater)
        val view = registerActivityBinding.root
        setContentView(view)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        initClickListener()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    private fun initClickListener() {
        registerActivityBinding.btnRegister.setOnClickListener {
            if (registerFormIsValid()) {
                Log.d("RegisterActivity", "BTN register clicked.")
                registerViewModel.checkUsername(registerActivityBinding.etUsername.text.toString())
                registerViewModel.usernameStatus.observe(this, Observer { response ->
                    when (response) {
                        is Resource.Success -> {
                            registerViewModel.doRegister(
                                registerActivityBinding.etFullName.text.toString(),
                                registerActivityBinding.etUsername.text.toString(),
                                registerActivityBinding.etRegisterPassword.text.toString()
                            )

                            Toast.makeText(this@RegisterActivity, "User created successfully.", Toast.LENGTH_SHORT).show()

                            registerActivityBinding.etFullName.setText("")
                            registerActivityBinding.etUsername.setText("")
                            registerActivityBinding.etRegisterPassword.setText("")
                        }

                        is Resource.Error -> {
                            Toast.makeText(this@RegisterActivity, response.message, Toast.LENGTH_SHORT).show()
                        }

                        is Resource.Loading -> {
                            // in loading state
                        }
                    }
                })
            }
        }
    }

    private fun registerFormIsValid(): Boolean {
        if (registerActivityBinding.etFullName.text.toString().isNullOrEmpty()) {
            Toast.makeText(
                this@RegisterActivity,
                "Full name field is empty.",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        if (registerActivityBinding.etUsername.text.toString().isNullOrEmpty()) {
            Toast.makeText(
                this@RegisterActivity,
                "Username field is empty.",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        if (registerActivityBinding.etRegisterPassword.text.toString().isNullOrEmpty()) {
            Toast.makeText(
                this@RegisterActivity,
                "Password field is empty.",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        return true
    }

}