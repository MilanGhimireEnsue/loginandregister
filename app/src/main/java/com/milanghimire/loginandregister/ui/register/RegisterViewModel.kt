package com.milanghimire.loginandregister.ui.register

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.milanghimire.loginandregister.entities.User
import com.milanghimire.loginandregister.repository.AppDbRepository
import com.milanghimire.loginandregister.utils.Resource
import kotlinx.coroutines.launch
import java.lang.Exception

class RegisterViewModel @ViewModelInject constructor(
    private val appDbRepository: AppDbRepository
) : ViewModel() {

    var usernameStatus: MutableLiveData<Resource<String>> = MutableLiveData()

    fun doRegister(fullName: String, username: String, password: String) = viewModelScope.launch {
        try {
            val userObj = User(fullName, username, password)
            appDbRepository.insertUser(userObj)
        } catch (ex: Exception) {
            Log.d("RegisterViewModel", "Exception occurred: ${ex.message}")
        }
    }

    fun checkUsername(username: String) = viewModelScope.launch {
        try {
            usernameStatus.postValue(Resource.Loading())
            if (appDbRepository.checkUsername(username)) {
                Log.d("RegisterViewModel", "username found.")
                usernameStatus.postValue(Resource.Error("Username found"))
            } else {
                Log.d("RegisterViewModel", "username not found.")
                usernameStatus.postValue(Resource.Success("Username not found."))
            }
        } catch (ex: Exception) {
            Log.d("RegisterViewModel", "safeCheckUsername Exception occurred: ${ex.message}")
            usernameStatus.postValue(Resource.Error("Some error occurred."))
            usernameStatus.postValue(Resource.Loading())
        }
    }
}