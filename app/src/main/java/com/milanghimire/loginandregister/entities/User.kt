package com.milanghimire.loginandregister.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "user"
)
data class User(
    var fullName: String = "",
    var username: String = "",
    var password: String = ""
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
}
