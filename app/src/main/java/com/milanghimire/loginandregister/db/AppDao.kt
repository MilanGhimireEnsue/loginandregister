package com.milanghimire.loginandregister.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.milanghimire.loginandregister.entities.User

@Dao
interface AppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(user: User): Long

    @Query("SELECT EXISTS(SELECT 1 FROM user WHERE username=:username)")
    suspend fun checkUsername(username: String): Boolean

    @Query("SELECT * FROM user")
    fun getAllUsers(): LiveData<List<User>>

    @Query("SELECT COUNT(*) FROM user WHERE username=:username AND password=:password")
    suspend fun getUser(username: String, password: String): Int
}
