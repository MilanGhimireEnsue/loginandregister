package com.milanghimire.loginandregister.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.milanghimire.loginandregister.entities.User

@Database(
    entities = [User::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getAppDao(): AppDao
}