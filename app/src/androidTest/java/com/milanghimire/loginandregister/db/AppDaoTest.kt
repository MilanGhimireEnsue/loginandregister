package com.milanghimire.loginandregister.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertThat
import com.milanghimire.loginandregister.entities.User
import com.milanghimire.loginandregister.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class AppDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var dao: AppDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        ).allowMainThreadQueries().build()
        dao = database.getAppDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun insertUser() = runBlockingTest {
        val userObj = User("ram ram ram", "ram", "ram")
        dao.upsert(userObj)

        /*
        * since getAllUsers uses LiveData we use getOrAwaitValue to wait for the result
        * which provides the List<User> instead of LiveData<List<User>>
        * */
        val allUsers = dao.getAllUsers().getOrAwaitValue()

        assertThat(allUsers).contains(userObj)
    }

    @Test
    fun getUser() = runBlockingTest {
        val userObj = User("ram ram ram", "ram", "ram")
        dao.upsert(userObj)

        /*
        * since getAllUsers uses LiveData we use getOrAwaitValue to wait for the result
        * which provides the List<User> instead of LiveData<List<User>>
        * */
        val result = dao.getUser("ram", "ram")

        assertThat(result).isEqualTo(1)
    }

    @Test
    fun checkUsernameExists() = runBlockingTest {
        val userObj = User("ram ram ram", "ram", "ram")
        dao.upsert(userObj)

        /*
        * since getAllUsers uses LiveData we use getOrAwaitValue to wait for the result
        * which provides the List<User> instead of LiveData<List<User>>
        * */
        val result = dao.checkUsername("ram")

        assertThat(result).isEqualTo(true)
    }
}